import L from 'leaflet';

const initialState = {
    center: {
        lon: 17.4876264,
        lat: 63.8908987,
    },
    zoom: 3.38
};

var map = L.map('map').setView(initialState.center, initialState.zoom);

// add the OpenStreetMap tiles
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
}).addTo(map);

// show the scale bar on the lower left corner
L.control.scale().addTo(map);

/*
Create a custom icon in leaflet to represent santa
*/
var santaIcon = L.icon({
    iconUrl: 'image/santa.png',
    iconSize: [40, 40], // size of the icon
    iconAnchor: [20, 20], // point of the icon which will correspond to marker's location
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

/*
Create a marker to display santa's home with custom icon
*/
let santaMarker = L.marker({ lat: 0, lon: 0 }, { icon: santaIcon });
santaMarker.bindPopup("Santa's home. Ohohoh!")
santaMarker.addTo(map);

export default santaMarker;
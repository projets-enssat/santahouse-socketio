/*
Simple object to count santa's house position
*/
const santaHouse = {
    lat: 0,
    lon: 0,
    index: 0,

    addPosition(data) {
        this.lat += data.latitude;
        this.lon += data.longitude;
        this.index++;
    },

    latitude() {
        return (this.index === 0) ? 0 : this.lat / this.index;
    },

    longitude() {
        return (this.index === 0) ? 0 : this.lon / this.index;
    },

    position() {
        return {
            lon: this.longitude(),
            lat: this.latitude()
        }
    }
}

export default santaHouse;
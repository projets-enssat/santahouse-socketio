import io from 'socket.io-client';
import santaMarker from './map';
import santaHouse from './santa';


const socket = io("https://boiling-fortress-23587.herokuapp.com/")

/*
SocketIO listener to update santa's position by working out average
*/
socket.on('santa', (data) => {
    // add possible santa home location to the counter
    let possibleLocation = JSON.parse(data);
    santaHouse.addPosition(possibleLocation);
    // update home marker position
    santaMarker.setLatLng(santaHouse.position());
})

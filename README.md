# Santa's house

> Simple project using leaflet and websocket to determine santa's house


## Build the project

This project use webpack to bundle required js library in a single file included in index.html.

First, install project's dependencies
```bash
npm install
```

Then you can use
```bash
npm run build
```

Alternatively, you can run a development server with hot reload enabled by doing :
```bash
npm run serve
```


## Distribute

Once the project is built, you just have to hosts static files (in folder `./public`) on a web server. For example by running
```bash
cd public
python3 -m http.server
```


## Where is Santa?

A socketio server sends Santa's coordinates every seconds through a websocket, in a message called 'santa'.

https://boiling-fortress-23587.herokuapp.com/

* connect to the websocket with socketIO
* catch messages called 'santa' (`{'latitude':xx.xxx,'longitude':yy.yyy}`)
* for each message, put Santa's coordinates on a map with the Leaflet API.
* You can put Santa's house on the map by rougthly compute average latitude and longitude.
* Soon, you'll find that Santa is never to far from his house. 
* Can you spot it? No? Why don't you use some help from Maching Learning ?
